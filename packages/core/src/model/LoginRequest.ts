interface LoginRequest {
    email: string
    password: string
    deviceName: string
    expire?: boolean
}

export default LoginRequest