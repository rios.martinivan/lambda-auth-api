interface User {
    id?: number
    full_name: string
    email: string
    password_hash: string
}

export default User