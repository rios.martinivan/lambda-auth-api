interface Privilege {
    id?: number
    name: string
}

export default Privilege