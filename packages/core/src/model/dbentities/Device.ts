interface Device {
    id?: number
    name: string
    disabled: boolean
    user_id: number
}

export default Device