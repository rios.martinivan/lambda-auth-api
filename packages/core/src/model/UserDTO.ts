import bcrypt from "bcrypt"
import User from "./dbentities/User"
import {ServerError} from "./Errors"

class UserDTO {
    readonly full_name: string
    readonly email: string
    readonly password: string

    constructor(full_name: string, email: string, password: string) {
        this.full_name = full_name
        this.email = email
        this.password = password
    }

    async toEntity(): Promise<User> {
        try {
            return {
                full_name: this.full_name,
                email: this.email,
                password_hash: await bcrypt.hash(this.password, 10),
            }
        } catch (e) {
            throw new ServerError()
        }
    }
}

export default UserDTO