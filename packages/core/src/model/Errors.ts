interface HTTPEntity {
    statusCode: number
    body: any
}

export class BaseHTTPError extends Error {
    readonly code: number

    constructor(message: string, code: number) {
        super(message)
        this.code = code
    }

    toHTTPEntity(): HTTPEntity {
        return {
            statusCode: this.code,
            body: this.message,
        }
    }
}

export class BadRequest extends BaseHTTPError {
    constructor(message: string = "Bad request") {
        super(message, 400)
    }
}

export class NotFound extends BaseHTTPError {
    constructor(message: string = "Not Found") {
        super(message, 404)
    }
}

export class ServerError extends BaseHTTPError {
    constructor(message: string = "Server Error") {
        super(message, 500)
    }
}

export class Unauthorized extends BaseHTTPError {
    constructor(message: string = "Unauthorized") {
        super(message, 401)
    }
}
