import User from "./dbentities/User"
import Device from "./dbentities/Device"
import Privilege from "./dbentities/Privilege"
import Role from "./dbentities/Role"

interface Database {
    users: User
    users_roles: UsersRoles
    devices: Device
    privileges: Privilege
    roles: Role
    roles_privileges: RolesPrivileges
}

interface UsersRoles {
    user_id: number
    role_id: number
}

interface RolesPrivileges {
    role_id: number
    privilege_id: number
}

export default Database