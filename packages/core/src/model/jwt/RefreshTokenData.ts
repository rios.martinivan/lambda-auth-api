import AccessToken from "./AccessToken"
import Claims from "./Claims"
import {Unauthorized} from "../Errors"

class RefreshTokenData {
    readonly userID: number
    readonly deviceName: string

    constructor(token: string, secret: string) {
        const claims = new AccessToken(token).parseClaims(secret) as Claims
        if (claims.scopes.length === 0 || !claims.scopes.includes("REFRESH_TOKEN"))
            throw new Unauthorized("Invalid REFRESH_TOKEN")

        this.userID = claims.userID
        this.deviceName = claims.deviceName
    }
}

export default RefreshTokenData