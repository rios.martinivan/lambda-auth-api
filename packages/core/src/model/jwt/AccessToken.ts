import jwt, { Secret } from 'jsonwebtoken'
import {Unauthorized} from "../Errors"

class AccessToken {
    private readonly token: string

    constructor(token: string) {
        this.token = token
    }

    parseClaims(secret: Secret) {
        try {
            return jwt.verify(this.token, secret)
        } catch (e) {
            throw new Unauthorized()
        }
    }
}

export default AccessToken