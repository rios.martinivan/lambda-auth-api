interface Claims {
    userID: number
    deviceName: string
    scopes: string[]
}

export default Claims