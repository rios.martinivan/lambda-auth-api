import Claims from "../model/jwt/Claims"
import jwt from "jsonwebtoken"
import getUuidByString from "uuid-by-string"

export function createAccessToken(claims: Claims, secretKey: string) {
    const userIDDevice = `${claims.userID}-${claims.deviceName}`

    return jwt.sign(claims, secretKey, {
        subject: claims.userID!.toString(),
        jwtid: getUuidByString(userIDDevice),
        expiresIn: '6h',
    })
}

export function createRefreshToken(claims: Claims, secretKey: string, expire?: boolean) {
    const userIDDevice = `${claims.userID}-${claims.deviceName}`

    const parsedClaims: Claims = {
        ...claims,
        scopes: ["REFRESH_TOKEN"],
    }
    return jwt.sign(parsedClaims, secretKey, {
        subject: claims.userID!.toString(),
        issuer: "test.com",
        jwtid: getUuidByString(userIDDevice),
        expiresIn: expire ? '24h' : undefined,
    })
}