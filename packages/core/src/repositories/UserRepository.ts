import {Kysely} from "kysely"
import Database from "../model/Database"
import User from "../model/dbentities/User"
import UserDTO from "../model/UserDTO"

const TABLE = "users"

class UserRepository {
    private readonly db: Kysely<Database>

    constructor(db: Kysely<Database>) {
        this.db = db
    }

    async create(userDTO: UserDTO): Promise<number> {
        const entity = await userDTO.toEntity()
        const result = await this.db
            .insertInto(TABLE)
            .values(entity)
            .returning("id")
            .executeTakeFirstOrThrow()

        return result.id!
    }

    async addRole(userID: number, roleID: number) {
        await this.db
            .insertInto("users_roles")
            .values({
                user_id: userID,
                role_id: roleID,
            })
            .execute()
    }

    async findByEmail(email: string): Promise<User | undefined> {
        return await this.db
            .selectFrom(TABLE)
            .select(["id", "full_name", "email", "password_hash"])
            .where("email", "=", email)
            .executeTakeFirst()
    }

    async findByID(id: number): Promise<User | undefined> {
        return await this.db
            .selectFrom(TABLE)
            .select(["id", "full_name", "email", "password_hash"])
            .where("id", "=", id)
            .executeTakeFirst()
    }

    async existsById(id: number) {
        const user = await this.db
            .selectFrom(TABLE)
            .select(["id"])
            .where("id", "=", id)
            .executeTakeFirst()
        return user !== undefined && user.id !== undefined
    }
}

export default UserRepository