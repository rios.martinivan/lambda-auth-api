import {Kysely} from "kysely"
import Database from "../model/Database"
import Role from "../model/dbentities/Role"

const TABLE = "roles"

class RoleRepository {
    private readonly db: Kysely<Database>

    constructor(db: Kysely<Database>) {
        this.db = db
    }

    async findByName(name: string): Promise<Role | undefined> {
        return await this.db
            .selectFrom(TABLE)
            .select(["roles.id", "roles.name"])
            .where("name", "=", name)
            .executeTakeFirst()
    }

    async findAllByUserID(userID: number): Promise<Role[]> {
        return await this.db
            .selectFrom(TABLE)
            .leftJoin("users_roles", "users_roles.role_id", "roles.id")
            .select(["roles.id", "roles.name"])
            .where("users_roles.user_id", "=", userID)
            .execute()
    }
}

export default RoleRepository