import {Kysely} from "kysely"
import Database from "../model/Database"
import Privilege from "../model/dbentities/Privilege"

const TABLE = "privileges"

class PrivilegeRepository {
    private readonly db: Kysely<Database>

    constructor(db: Kysely<Database>) {
        this.db = db
    }

    async findAllByRoleID(roleID: number): Promise<Privilege[]> {
        return await this.db
            .selectFrom(TABLE)
            .leftJoin("roles_privileges", "roles_privileges.privilege_id", "privileges.id")
            .select(["privileges.id", "privileges.name"])
            .where("roles_privileges.role_id", "=", roleID)
            .execute()
    }
}

export default PrivilegeRepository