import {Kysely, sql} from "kysely"
import Database from "../model/Database"
import Device from "../model/dbentities/Device"

const TABLE = "devices"

class DeviceRepository {
    private readonly db: Kysely<Database>

    constructor(db: Kysely<Database>) {
        this.db = db
    }

    async create(userID: number, deviceName: string) {
        await sql`
            merge INTO "${TABLE}" USING (VALUES (${userID}, ${deviceName})) AS i (name, user_id)
            ON "${TABLE}"."name" = "i"."name" AND "${TABLE}"."user_id" = "i"."user_id"
            WHEN matched THEN
                UPDATE SET "disabled" = false,
            WHEN NOT matched THEN
                INSERT (name, disabled, user_id) VALUES (${userID}, false, ${deviceName}) 
        `.execute(this.db)
    }

    async findByUserIDAndName(userID: number, name: string): Promise<Device | undefined> {
        return await this.db
            .selectFrom(TABLE)
            .select(["id", "name", "disabled", "user_id"])
            .where("user_id", "=", userID)
            .where("name", "=", name)
            .executeTakeFirst()
    }
}

export default DeviceRepository