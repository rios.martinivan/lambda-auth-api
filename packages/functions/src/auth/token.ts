import { Kysely } from "kysely"
import { DataApiDialect } from "kysely-data-api"
import { RDSData } from "@aws-sdk/client-rds-data"
import { RDS } from "sst/node/rds"
import Database from "@auth-api/core/model/Database"
import {APIGatewayProxyHandlerV2} from "aws-lambda"
import {BadRequest, BaseHTTPError, Unauthorized} from "@auth-api/core/model/Errors"
import UserRepository from "@auth-api/core/repositories/UserRepository"
import RoleRepository from "@auth-api/core/repositories/RoleRepository"
import Claims from "@auth-api/core/model/jwt/Claims"
import PrivilegeRepository from "@auth-api/core/repositories/PrivilegeRepository"
import { Config } from "sst/node/config"
import DeviceRepository from "@auth-api/core/repositories/DeviceRepository"
import RefreshTokenData from "@auth-api/core/model/jwt/RefreshTokenData"
import {createAccessToken} from "@auth-api/core/util/Tokens"


const db = new Kysely<Database>({
    dialect: new DataApiDialect({
        mode: "postgres",
        driver: {
            database: RDS.Cluster.defaultDatabaseName,
            secretArn: RDS.Cluster.secretArn,
            resourceArn: RDS.Cluster.clusterArn,
            client: new RDSData({}),
        },
    }),
})

const userRepository = new UserRepository(db)
const roleRepository = new RoleRepository(db)
const privilegeRepository = new PrivilegeRepository(db)
const deviceRepository = new DeviceRepository(db)

export const main: APIGatewayProxyHandlerV2 = async(event) => {
    try {
        const data = JSON.parse(event.body!) as { token: string }

        const secretKey = Config.JWT_KEY
        const tokenData = new RefreshTokenData(data.token, secretKey)

        const device = await deviceRepository.findByUserIDAndName(tokenData.userID, tokenData.deviceName)
        if (!device || device.disabled) throw new Unauthorized("Token disabled")

        const userExists = userRepository.existsById(tokenData.userID)
        if (!userExists) throw new Unauthorized("User does not exist")

        const roles = await roleRepository.findAllByUserID(tokenData.userID)
        const privileges = (await Promise.all(
            roles.map((role) => privilegeRepository.findAllByRoleID(role.id!))
        )).reduce((acc, value) => acc.concat(value), [])

        const claims: Claims = {
            userID: tokenData.userID,
            deviceName: tokenData.deviceName,
            scopes: privileges.map((privilege) => privilege.name),
        }

        const accessToken = createAccessToken(claims, secretKey)

        return {
            statusCode: 200,
            body: {
                token: accessToken,
                privileges: claims.scopes,
            }
        }
    } catch (e) {
        if (e instanceof BaseHTTPError) return e.toHTTPEntity()

        const error = new BadRequest()
        return error.toHTTPEntity()
    }
}
