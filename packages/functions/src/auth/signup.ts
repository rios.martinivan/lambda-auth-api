import { Kysely } from "kysely"
import { DataApiDialect } from "kysely-data-api"
import { RDSData } from "@aws-sdk/client-rds-data"
import { RDS } from "sst/node/rds"
import Database from "@auth-api/core/model/Database"
import {APIGatewayProxyHandlerV2} from "aws-lambda"
import UserDTO from "@auth-api/core/model/UserDTO"
import {BadRequest, BaseHTTPError, ServerError} from "@auth-api/core/model/Errors"
import UserRepository from "@auth-api/core/repositories/UserRepository"
import RoleRepository from "@auth-api/core/repositories/RoleRepository"

const db = new Kysely<Database>({
    dialect: new DataApiDialect({
        mode: "postgres",
        driver: {
            database: RDS.Cluster.defaultDatabaseName,
            secretArn: RDS.Cluster.secretArn,
            resourceArn: RDS.Cluster.clusterArn,
            client: new RDSData({}),
        },
    }),
})

const userRepository = new UserRepository(db)
const roleRepository = new RoleRepository(db)

export const main: APIGatewayProxyHandlerV2 = async(event) => {
    try {
        const data = JSON.parse(event.body!) as UserDTO
        const userID = await userRepository.create(data)
        const defaultRoleID = await roleRepository.findByName("user")
        if (!defaultRoleID) throw new ServerError()

        await userRepository.addRole(userID, defaultRoleID.id!)
    } catch (e) {
        if (e instanceof BaseHTTPError) return e.toHTTPEntity()

        const error = new BadRequest()
        return error.toHTTPEntity()
    }

    return {
        statusCode: 201,
    }
}
