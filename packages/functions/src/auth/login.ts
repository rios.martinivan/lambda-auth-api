import { Kysely } from "kysely"
import { DataApiDialect } from "kysely-data-api"
import { RDSData } from "@aws-sdk/client-rds-data"
import { RDS } from "sst/node/rds"
import Database from "@auth-api/core/model/Database"
import {APIGatewayProxyHandlerV2} from "aws-lambda"
import {BadRequest, BaseHTTPError, Unauthorized} from "@auth-api/core/model/Errors"
import UserRepository from "@auth-api/core/repositories/UserRepository"
import RoleRepository from "@auth-api/core/repositories/RoleRepository"
import LoginRequest from "@auth-api/core/model/LoginRequest"
import Claims from "@auth-api/core/model/jwt/Claims"
import PrivilegeRepository from "@auth-api/core/repositories/PrivilegeRepository"
import bcrypt from "bcrypt"
import { Config } from "sst/node/config"
import DeviceRepository from "@auth-api/core/repositories/DeviceRepository"
import {createAccessToken, createRefreshToken} from "@auth-api/core/util/Tokens"

const db = new Kysely<Database>({
    dialect: new DataApiDialect({
        mode: "postgres",
        driver: {
            database: RDS.Cluster.defaultDatabaseName,
            secretArn: RDS.Cluster.secretArn,
            resourceArn: RDS.Cluster.clusterArn,
            client: new RDSData({}),
        },
    }),
})

const userRepository = new UserRepository(db)
const roleRepository = new RoleRepository(db)
const privilegeRepository = new PrivilegeRepository(db)
const deviceRepository = new DeviceRepository(db)

export const main: APIGatewayProxyHandlerV2 = async(event) => {
    try {
        const data = JSON.parse(event.body!) as LoginRequest
        const user = await userRepository.findByEmail(data.email)
        if (!user) throw new Unauthorized("Authentication Failed. Wrong email or password.")

        const matches = await bcrypt.compare(data.password, user.password_hash)
        if (!matches)  throw new Unauthorized("Authentication Failed. Wrong email or password.")

        const roles = await roleRepository.findAllByUserID(user.id!)
        const privileges = (await Promise.all(
            roles.map((role) => privilegeRepository.findAllByRoleID(role.id!))
        )).reduce((acc, value) => acc.concat(value), [])

        const claims: Claims = {
            userID: user.id!,
            deviceName: data.deviceName,
            scopes: privileges.map((privilege) => privilege.name),
        }

        const secretKey = Config.JWT_KEY
        const accessToken = createAccessToken(claims, secretKey)
        const refreshToken = createRefreshToken(claims, secretKey, data.expire)
        await deviceRepository.create(user.id!, data.deviceName)

        return {
            statusCode: 200,
            body: {
                accessToken: {
                    token: accessToken,
                    privileges: claims.scopes,
                },
                refreshToken: refreshToken,
            }
        }
    } catch (e) {
        if (e instanceof BaseHTTPError) return e.toHTTPEntity()

        const error = new BadRequest()
        return error.toHTTPEntity()
    }
}
