import { Kysely } from "kysely"
import { DataApiDialect } from "kysely-data-api"
import { RDSData } from "@aws-sdk/client-rds-data"
import { RDS } from "sst/node/rds"
import Database from "@auth-api/core/model/Database"
import {APIGatewayProxyHandlerV2} from "aws-lambda"
import {BadRequest, BaseHTTPError, NotFound, Unauthorized} from "@auth-api/core/model/Errors"
import UserRepository from "@auth-api/core/repositories/UserRepository"
import Claims from "@auth-api/core/model/jwt/Claims"
import { Config } from "sst/node/config"
import AccessToken from "@auth-api/core/model/jwt/AccessToken"
import DeviceRepository from "@auth-api/core/repositories/DeviceRepository";

const db = new Kysely<Database>({
    dialect: new DataApiDialect({
        mode: "postgres",
        driver: {
            database: RDS.Cluster.defaultDatabaseName,
            secretArn: RDS.Cluster.secretArn,
            resourceArn: RDS.Cluster.clusterArn,
            client: new RDSData({}),
        },
    }),
})

const userRepository = new UserRepository(db)
const deviceRepository = new DeviceRepository(db)

export const main: APIGatewayProxyHandlerV2 = async(event) => {
    try {
        const token = event.headers["Authorization"]?.replace("Bearer ", "")
        if (!token) throw new Unauthorized("Authorization token not found")

        const secretKey = Config.JWT_KEY
        const claims = new AccessToken(token).parseClaims(secretKey) as Claims

        if (claims.scopes.includes("REFRESH_TOKEN")) throw new Unauthorized("You cannot use the refresh token to access this resource")

        const user = await userRepository.findByID(claims.userID)
        if (!user) throw new NotFound(`User not found with id: ${claims.userID}`)

        const device = await deviceRepository.findByUserIDAndName(claims.userID, claims.deviceName)
        if (!device || device.disabled) throw new Unauthorized("Token disabled")

        return {
            statusCode: 200,
            body: {
                user: {
                    full_name: user.full_name,
                    email: user.email,
                }
            }
        }
    } catch (e) {
        if (e instanceof BaseHTTPError) return e.toHTTPEntity()

        const error = new BadRequest()
        return error.toHTTPEntity()
    }
}
