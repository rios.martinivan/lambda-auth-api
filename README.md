# Test Project

<a href='https://gitlab.com/rios.martinivan/lambda-auth-api/-/pipelines?ref=main'><img src='https://gitlab.com/rios.martinivan/lambda-auth-api/badges/main/pipeline.svg'></a>
<a href='https://gitlab.com/rios.martinivan/lambda-auth-api/-/pipelines?ref=main'><img src='https://gitlab.com/rios.martinivan/lambda-auth-api/badges/main/coverage.svg'></a>

This project is an example of an authentication and authorization API using AWS Lambda via SST.  
Since we are using lambda, this example does not include configuration to work within a VPC, as we assume
you are trying to create a small MVP or proof of concept, and do not have a fully configured infrastructure.  
Also, JWTs are generated using a secret instead of a private key to avoid making the project more complex than necessary.

## Table of contents
- [How to run the project](#how-to-run-the-project)
- [Project Structure](#project-structure)
- [Routes](#routes)
- [Exception Handling](#exception-handling)
- [Other notes](#other-notes)
    - [Why AWS Lambda?](#why-aws-lambda)
    - [How authentication and authorization works?](#how-authentication-and-authorization-works)
    - [Why the project has no unit or integration tests?](#why-the-project-has-no-unit-or-integration-tests)
- [Authors and acknowledgment](#authors-and-acknowledgment)

***

## How to run the project

The project can be run in two ways:
- Run it locally through SST [live lambda development](https://docs.sst.dev/live-lambda-development) with:
  ```bash
  yarn dev
  ```
- [Deploy it](https://docs.sst.dev/learn/deploy-to-prod) to AWS Lambda and run it in a dev/stg/prod environment with:
```bash
yarn deploy --stage <dev/stg/prod>
```

## Project Structure

- [core folder](packages/core/src): Folder containing the majority of the business logic.
- [functions folder](packages/functions/src): Folder containing the lambda functions.
- [migrations folder](services/migrations): Folder containing SQL migrations.
- [stacks folder](stacks): Folder containing the project infrastructure-as-code (IaC).

## Routes

The project exposes 4 routes:
- `/auth/signup`: Endpoint used by a new user to register.
- `/auth/login`: Endpoint used to log in, returns a token and a refresh token. To learn more about these tokens, please read [How authentication and authorization works?](#how-authentication-and-authorization-works).
- `/auth/token`: Endpoint used to generate a new token from a refresh token.
- `/user`: Endpoint that returns the current user based on the token sent in the Authorization header.

## Exception Handling

The lambda functions catch any exceptions and return their corresponding HTTP code.
Also, [custom exceptions](packages/core/src/model/Errors.ts) were created for the most common HTTP errors: BadRequest,
NotFound, ServerError, and Unauthorized.

## Other notes

Here we will mention some more technical data about the project, which is important to take into account.

<details open>
<summary>Why AWS Lambda?</summary>

### Why AWS Lambda?

Lambda is useful in cases where you need to expose code that can scale quickly, and de-scale to 0 when it's not being
used. On the other hand, Lambda is a bad choice for creating APIs that are going to be called all the time, as it can
become very expensive very quickly.

It's generally a good idea to use Lambda when you want to quickly put together a proof-of-concept, a small MVP,
or when you need to run functions every so often with a cron.  
However, remember that there are other services that may give you better results depending on what you want
to do. AWS ECS, for example, allows you to deploy and expose services in Docker containers, and each instance
will be able to process several requests unlike Lambda, where you need one instance per request.

</details>

<details open>
<summary>How authentication and authorization works?</summary>

### How authentication and authorization works?

The authentication process starts when a registered user calls the login endpoint `/auth/login`. When
this happens, the API will validate the user email and password and generate two JWT token containing the
user id, the name of the device used to log in, and all the privileges the user has based on his account roles. 
It is important to mention that the JWT token is digitally signed by a private key or a secret key, which allows
us to validate that this token will not be modified by a third party in the future.

Finally, the login process ends by returning an access token and a refresh token to the user.  
Now the user will be able to access endpoints that he could not access before by sending his access token in the Authorization header.

#### But why two tokens?

The access token is a short-lived token that will expire within 6 hours. After the token expires, all requests
made with that token will return 403 Unauthorized.  
When this happens, the user has to call the `/auth/token` endpoint with his refresh token, and he will be
given a new access token.  
We do this mainly to refresh the privileges embedded within the access token.

The refresh token, on the other hand, can expire in 24 hours or never if the user uses the `expires: false` option
when logging in. If the refresh token expires the user will be forced to log in again.

Finally, the user can disable a device and block the tokens that have been generated with that device name.

#### Authentication is awesome, but what about authorization?

Since tokens have a list of privileges that the user has, access to certain endpoints can be allowed or prevented
without even needing to access the database, this allows incorrect requests to be filtered out in a gateway
without them even reaching the API.

In addition, different endpoints can use the privileges present in the access token to know whether a user can access
certain resources or not.

</details>

<details open>
<summary>Why the project has no unit or integration tests?</summary>

### Why the project has no unit or integration tests?

Normally, we should always create unit and integration tests for our code, but SST needs to access and
use an AWS account in order to run tests, this means that you cannot run tests completely locally.
This project was created as an example, so I prefer not to add things that don't work without
setting up external dependencies.

If you need a production-ready project, please create some tests and use a CI/CD pipeline.

</details>

## Authors and acknowledgment

- [Martin Ivan Rios](https://linkedin.com/in/ivr2132)
