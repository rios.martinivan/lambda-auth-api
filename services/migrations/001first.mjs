import { Kysely } from "kysely"

/**
 * @param db {Kysely<any>}
 */
export async function up(db) {
    // Users table
    await db.schema
        .createTable("users")
        .addColumn("id", "serial", (col) =>
            col.autoIncrement().primaryKey())
        .addColumn("full_name", "varchar", (col) => col.notNull())
        .addColumn("email", "varchar", (col) => col.notNull())
        .addColumn("password_hash", "varchar", (col) => col.notNull())
        .execute()
    await db.schema
        .createIndex("users_email_index")
        .on("users")
        .columns(["email"])
        .execute()

    // Devices table
    await db.schema
        .createTable("devices")
        .addColumn("id", "serial", (col) =>
            col.autoIncrement().primaryKey())
        .addColumn("name", "varchar", (col) => col.notNull())
        .addColumn("disabled", "boolean", (col) => col.notNull())
        .addColumn("user_id", "bigint", (col) =>
            col.notNull().references("users.id").onDelete("cascade"))
        .execute()

    // Privileges table
    await db.schema
        .createTable("privileges")
        .addColumn("id", "serial", (col) =>
            col.autoIncrement().primaryKey())
        .addColumn("name", "varchar", (col) => col.notNull())
        .execute()
    await db.schema
        .createIndex("privileges_name_index")
        .on("privileges")
        .columns(["name"])
        .execute()

    // Roles table
    await db.schema
        .createTable("roles")
        .addColumn("id", "serial", (col) =>
            col.autoIncrement().primaryKey())
        .addColumn("name", "varchar", (col) => col.notNull())
        .execute()
    await db.schema
        .createIndex("roles_name_index")
        .on("roles")
        .columns(["name"])
        .execute()

    // Roles-privileges junction table
    await db.schema
        .createTable("roles_privileges")
        .addColumn("role_id", "bigint", (col) =>
            col.notNull().references("roles.id").onDelete("cascade"))
        .addColumn("privilege_id", "bigint", (col) =>
            col.notNull().references("privileges.id").onDelete("cascade"))
        .addPrimaryKeyConstraint('primary_key', ['role_id', 'privilege_id'])
        .execute()

    // Users-roles junction table
    await db.schema
        .createTable("users_roles")
        .addColumn("user_id", "bigint", (col) =>
            col.notNull().references("users.id").onDelete("cascade"))
        .addColumn("role_id", "bigint", (col) =>
            col.notNull().references("roles.id").onDelete("cascade"))
        .addPrimaryKeyConstraint('primary_key', ['user_id', 'role_id'])
        .execute()

    // Add base privileges
    const userCreatePrivID = await addPrivilege(db, "users:create")
    const userReadPrivID = await addPrivilege(db, "users:read")
    const userUpdatePrivID = await addPrivilege(db, "users:update")
    const userDeletePrivID = await addPrivilege(db, "users:delete")

    // Add base roles
    await addRole("admin", [userCreatePrivID, userReadPrivID, userUpdatePrivID, userDeletePrivID])
    await addRole("user", [userReadPrivID])
}

async function addPrivilege(db, name) {
    const result = await db
        .insertInto("privilege")
        .values({
            name: name,
        })
        .returning("id")
        .executeTakeFirstOrThrow()
    return result.id
}

async function addRole(db, name, privilege_ids) {
    const result = await db
        .insertInto("roles")
        .values({
            name: name,
        })
        .returning("id")
        .executeTakeFirstOrThrow()

    for (const privilege_id of privilege_ids) {
        await db
            .insertInto("roles_privileges")
            .values({
                role_id: result.id,
                privilege_id: privilege_id,
            })
            .execute();
    }
}

/**
 * @param db {Kysely<any>}
 */
export async function down(db) {
    // Drop users table
    await db.schema.dropIndex("users_email_index").execute()
    await db.schema.dropTable("users").execute()

    // Drop privileges table
    await db.schema.dropIndex("privileges_name_index").execute()
    await db.schema.dropTable("privileges").execute()

    // Drop roles table
    await db.schema.dropIndex("roles_name_index").execute()
    await db.schema.dropTable("roles").execute()
}

module.exports = { up, down };