import { Api, RDS, StackContext } from "sst/constructs"
import {RDSProps} from "sst/constructs/RDS"

function MainStack({ stack, app }: StackContext) {
    const prodConfig = {
        autoPause: false,
        minCapacity: "ACU_8",
        maxCapacity: "ACU_64",
    } as RDSProps["scaling"]
    const devConfig = {
        autoPause: true,
        minCapacity: "ACU_2",
        maxCapacity: "ACU_2",
    } as RDSProps["scaling"]

    const cluster = new RDS(stack, "Cluster", {
        engine: "postgresql13.9",
        defaultDatabaseName: "auth",
        migrations: "services/migrations",
        scaling: app.stage === "prod" ? prodConfig : devConfig,
    })

    const api = new Api(stack, "Api", {
        defaults: {
            function: {
                bind: [cluster],
            },
        },
        routes: {
            "POST /auth/signup": "packages/functions/src/auth/signup.main",
            "POST /auth/login": "packages/functions/src/auth/login.main",
            "GET /auth/token": "packages/functions/src/auth/token.main",
            "GET /user": "packages/functions/src/user.main",
        },
    })

    stack.addOutputs({
        ApiEndpoint: api.url,
        SecretArn: cluster.secretArn,
        ClusterIdentifier: cluster.clusterIdentifier,
    })
}

export default MainStack